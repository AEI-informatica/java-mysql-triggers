/**
 * 
 */
package nl.avans.informatica.java;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Date;

/**
 * Example program to show how MySQL triggers are handled in a Java program.
 * 
 *
 */
public class MySqlConstraints {

	private Connection connect = null;
	private Statement statement = null;
	private PreparedStatement preparedStatement = null;
	private ResultSet resultSet = null;
	private String query = "";
	
	// Settings voor connectie met database
	private final String dtb_host = "localhost";
	private final String dtb_name = "bibliotheek";
	private final String dtb_user = "root";
	private final String dtb_password = "";

	// Connectionstring to the database
	private final String dbConnection = "jdbc:mysql://" + dtb_host + "/" + dtb_name + "?user=" + dtb_user; // + "&password=" + dtb_password;

	/**
	 * Demo of the several possibilities of querying the database.
	 * 
	 * @throws Exception
	 */
	public void useDataBase() {
		
		try {
			// This will load the MySQL driver; each different DB provider has its own driver
			Class.forName("com.mysql.jdbc.Driver");
			System.out.println("Successfully found the database driver");
		} catch (Exception ex) {
			System.out.println("Exception: " + ex.getMessage());
		}
			
		try {
			// Setup the connection with the DB
			connect = DriverManager.getConnection(dbConnection);
			System.out.println("Successfully connected to the database");
			// Statements allow to issue SQL queries to the database
			statement = connect.createStatement();
		} catch (Exception ex) {
			System.out.println("Exception: " + ex.getMessage());
		}
		System.out.println();
			

		// Issue a select statement to the database.
		// Resultset gets the result of the SQL query
		try {
			query = "SELECT * FROM `loan` WHERE MemberID=15251";
			System.out.println(query);
			resultSet = statement.executeQuery(query);
			writeResultSet(resultSet);
			// writeMetaData(resultSet);
		} catch (Exception ex) {
			System.out.println("Exception: " + ex.getMessage());
		}
		System.out.println();

		// De tweede uitlening voor hetzelfde exemplaar MAG NIET LUKKEN!
		// Exemplaar is nog niet geretourneerd.
		// INSERT INTO `loan` (`LoanDate`, `MemberID`, `CopyID`) VALUES (NOW(), 15251, 1071)
		try {
			// PreparedStatements can use variables and are more efficient
			// query = "insert into bestelling values (default, ?, ?, ?, ? , ?, ?)";
			query = "INSERT INTO `loan` (`LoanDate`, `MemberID`, `CopyID`) VALUES (now(), ?, ?)";
			preparedStatement = connect.prepareStatement(query);
			preparedStatement.setInt(1, 15251);
			preparedStatement.setInt(2, 1071);
			System.out.println(preparedStatement);
			preparedStatement.executeUpdate();
		} catch (Exception ex) {
			System.out.println("Exception: " + ex.getMessage());
		}
		System.out.println();

		// Test updates op de loan tabel.
		// Zet de ReturnedDate op een datum vóór de LoanDate
		// Als het goed is lukt dit niet en volgt een foutmelding.
		// UPDATE `loan` SET `ReturnedDate`='2000-01-01 00:00:00' WHERE MemberID=15251 AND CopyID=1071;
		try {
			// PreparedStatements can use variables and are more efficient
			// query = "insert into bestelling values (default, ?, ?, ?, ? , ?, ?)";
			query = "UPDATE `loan` SET `ReturnedDate`='2000-01-01 00:00:00' WHERE MemberID=? AND CopyID=?";
			preparedStatement = connect.prepareStatement(query);
			preparedStatement.setInt(1, 15251);
			preparedStatement.setInt(2, 1071);
			System.out.println(preparedStatement);
			preparedStatement.executeUpdate();
		} catch (Exception ex) {
			System.out.println("Exception: " + ex.getMessage());
		}
		System.out.println();

		// Finally, close the connection to the database.
		close();
	}

	/**
	 * Example of how to print metadata from a resultset.
	 * Notice that we don't know the exact columnnames and amount of columns;
	 * this is extracted from the resultset.
	 * 
	 * @param resultSet
	 * @throws SQLException
	 */
	private void writeMetaData(ResultSet resultSet) throws SQLException {

		System.out.println("The columns in the table are: ");

		System.out.println("Table: " + resultSet.getMetaData().getTableName(1));
		for (int i = 1; i <= resultSet.getMetaData().getColumnCount(); i++) {
			System.out.println("Column " + i + " "
					+ resultSet.getMetaData().getColumnName(i));
		}
	}

	/**
	 * Write the contents of a resultset.
	 * Notice that we simply print all columns and values in the resultset.
	 * 
	 * @param resultSet
	 * @throws SQLException
	 */
	private void writeResultSet(ResultSet resultSet) throws SQLException {
		
		int columnCount = resultSet.getMetaData().getColumnCount();		
		System.out.println("\nTable: " + resultSet.getMetaData().getTableName(1));

		while (resultSet.next()) {

			for (int i = 1; i <= columnCount; i++) {
				System.out.print(resultSet.getMetaData().getColumnName(i) + ": " + resultSet.getString(i) + "\t");
			}
			System.out.println();
		}
	}

	/**
	 * You need to close the resultSet after its last use.
	 */
	private void close() {
		try {
			if (resultSet != null) {
				resultSet.close();
			}

			if (statement != null) {
				statement.close();
			}

			if (connect != null) {
				connect.close();
			}
		} catch (Exception e) {

		}
	}

	/**
	 * Main method to start the program.
	 * 
	 */
	public static void main(String[] args) {
		
		MySqlConstraints mySQL = new MySqlConstraints();
		mySQL.useDataBase();

	}

}
