-- --------------------------------------------------------
-- Relationele Databases 3 - Triggers
--
-- Dit bestand bevat een aantal triggers die in de les behandeld worden.
-- De triggers hebben betrekking op de bibliotheekdatabase.

DELIMITER //

-- --------------------------------------------------------
-- 
-- Stored procedures en stored functions
--
-- Een exemplaar kan alleen uitgeleend worden wanneer de lener niet 
-- meer dan 4 andere uitleningen open heeft staan.
--
DROP FUNCTION IF EXISTS amount_of_loans_for_member //
CREATE FUNCTION amount_of_loans_for_member( memID INT(6) unsigned )
	RETURNS INT
BEGIN
	DECLARE aantal INT;

	SELECT COUNT(*) 
	INTO aantal 
	FROM `loan` 
	WHERE `MemberID`=memID AND `ReturnedDate` IS NULL;

	RETURN aantal;
END //

-- Gebruik de functie als query
SELECT amount_of_loans_for_member(15251) AS 'Aantal uitleningen' //

-- --------------------------------------------------------
-- 
-- Trigger check_loandate
--
-- Controleert een uitlening voordat deze in de database wordt toegevoegd.
-- -- ReturnDate moet na LoanDate liggen
-- -- Exemplaar mag niet reeds uitgeleend zijn
-- -- (Exemplaar mag niet door andere lener gereserveerd zijn)
-- -- (Meer constraints zijn weggelaten)
-- 
DROP TRIGGER IF EXISTS `check_loandate` //
CREATE TRIGGER `check_loandate` 
BEFORE INSERT ON `loan`
FOR EACH ROW 
BEGIN 
	DECLARE msg varchar(255);
	
	-- De inleverdatum moet na de leendatum liggen.
	IF NEW.`ReturnedDate` < NEW.`LoanDate` THEN
		SET msg = 'Kan geen uitlening maken: einddatum ligt voor startdatum.'; 
		SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = msg;
	END IF; 

	-- Een uitgeleend exemplaar moet geretourneerd zijn 
	-- voordat het opnieuw uitgeleend kan worden.
	IF EXISTS(
		SELECT `LoanID` 
		FROM `loan` 
		WHERE `CopyID`=NEW.`CopyID` AND `ReturnedDate` IS NULL)
	THEN 
		SET msg = 'Kan geen uitlening maken: exemplaar is nog niet geretourneerd.'; 
		SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = msg; 
	END IF;

	-- Een lener mag maximaal 5 boeken lenen
	IF amount_of_loans_for_member(NEW.`MemberID`) >= 5
	THEN 
		SET msg = CONCAT('Lener ', NEW.`MemberID`, ' heeft al 5 of meer boeken.');
		SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = msg; 
	END IF;

END //

-- --------------------------------------------------------
-- 
-- Trigger check_update_loandate
--
-- Controleert een uitlening voordat deze in de database wordt toegevoegd.
-- -- ReturnDate moet na LoanDate liggen
-- -- Exemplaar mag niet reeds uitgeleend zijn
-- -- (Exemplaar mag niet door andere lener gereserveerd zijn)
-- -- (Meer constraints zijn weggelaten)
-- 
DROP TRIGGER IF EXISTS `check_update_loandate` //
CREATE TRIGGER `check_update_loandate` 
BEFORE UPDATE ON `loan`
FOR EACH ROW 
BEGIN 
	DECLARE msg varchar(255);
	
	IF NEW.`ReturnedDate` < NEW.`LoanDate` THEN
		SET msg = 'Kan uitlening niet updaten: einddatum ligt voor startdatum.'; 
		SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = msg;
	END IF; 
END //

DELIMITER ;

-- --------------------------------------------------------
-- 
-- Testqueries om te zien of onze triggers werken.
--

-- Reset aantal uitleningen, zodat je telkens met een schone lei begint.
DELETE FROM `loan` WHERE `MemberID` = 15251;

-- Ongeldige ReturnDate, zou niet moeten lukken.
INSERT INTO `loan` (`LoanDate`, `ReturnedDate`, `MemberID`, `CopyID`) VALUES
('2017-01-20 10:00:00', '2017-01-19 10:00:00', 15251, 1071);

-- Ongeldige ReturnDate, zou niet moeten lukken.
INSERT INTO `loan` (`LoanDate`, `ReturnedDate`, `MemberID`, `CopyID`) VALUES
('2017-01-20 10:00:00', '2017-01-19 10:00:00', 15251, 1071),	-- 1 dag eerder
('2017-01-20 10:00:00', '2016-01-19 10:00:00', 15251, 1071),	-- 1 jaar eerder
('2017-01-20 10:00:00', '2017-01-20 09:59:59', 15251, 1071);	-- 1 seconde eerder

-- De eerste uitlening voor een exemplaar moet lukken
INSERT INTO `loan` (`LoanDate`, `MemberID`, `CopyID`) VALUES (NOW(), 15251, 1071);

-- Laat zien dat het gelukt is
SELECT * FROM `loan` WHERE MemberID=15251 AND CopyID=1071;

-- De tweede uitlening voor hetzelfde exemplaar MAG NIET LUKKEN!
-- Exemplaar is nog niet geretourneerd.
INSERT INTO `loan` (`LoanDate`, `MemberID`, `CopyID`) VALUES (NOW(), 15251, 1071);

-- Test updates op de loan tabel.
-- Zet de ReturnedDate op een datum vóór de LoanDate
-- Als het goed is lukt dit niet en volgt een foutmelding.
UPDATE `loan` 
SET `ReturnedDate`='2000-01-01 00:00:00' 
WHERE MemberID=15251 AND CopyID=1071;

SELECT * FROM `loan` WHERE MemberID=15251 AND CopyID=1071;

-- Test 5 uitleningen - de 6e moet mislukken
INSERT INTO `loan` (`LoanDate`, `MemberID`, `CopyID`) VALUES (NOW(), 15251, 1061);
INSERT INTO `loan` (`LoanDate`, `MemberID`, `CopyID`) VALUES (NOW(), 15251, 1062);
INSERT INTO `loan` (`LoanDate`, `MemberID`, `CopyID`) VALUES (NOW(), 15251, 1063);
INSERT INTO `loan` (`LoanDate`, `MemberID`, `CopyID`) VALUES (NOW(), 15251, 1064);
INSERT INTO `loan` (`LoanDate`, `MemberID`, `CopyID`) VALUES (NOW(), 15251, 1065);
INSERT INTO `loan` (`LoanDate`, `MemberID`, `CopyID`) VALUES (NOW(), 15251, 1066);

-- Laat zien wat we tot nu toe hebben
SELECT * FROM `loan` WHERE MemberID=15251;
SELECT amount_of_loans_for_member(15251) AS 'Aantal uitleningen';


