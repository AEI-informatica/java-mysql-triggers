Avans Java MySql database connectie voorbeeld
====================================

About
-------------
Dit voorbeeld laat zien hoe je vanuit een Java programma met een MySql database kunt verbinden.
De database in dit voorbeeld is de Bibliotheek database die in de lessen Relationele Databases 2 en 3 is besproken.
De database implementeert een aantal triggers, zodat je kunt zien hoe Java op de MySql triggers reageert.

Building the project
--------------------
Je kunt deze code downloaden en als Maven project importeren in Netbeans of een andere Java IDE.

Om het voorbeeld te kunnen runnen moet je de bibliotheek database geïmporteert hebben, en moet 
de MySql database server gestart zijn.
